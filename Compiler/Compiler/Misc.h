#pragma once

#include "../Parser/LLParserTable.h"
#include "../Utils/format_utils.h"
#include "../Utils/stream_utils.h"
#include "../Utils/string_utils.h"
#include "../grammarlib/Grammar.h"
#include "../grammarlib/GrammarSymbol.h"

#include <ostream>

void WriteGrammar(const Grammar& grammar, std::ostream& out);
void WriteParserTable(const LLParserTable& parserTable, std::ostream& out);
